package com.appzmachine.typingspeedtest;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends AppCompatActivity {
    private Button button;
    private ImageView img1,img2,img3,img4,img5,img6;
    AdView adview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        button = findViewById(R.id.btn);
        img1 = findViewById(R.id.pimg1);
        img2 = findViewById(R.id.pimg2);
        img3 = findViewById(R.id.pimg3);
        img4 = findViewById(R.id.pimg4);
        img5 = findViewById(R.id.ehimg1);
        img6 = findViewById(R.id.ehimg2);
        adview = findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adview.loadAd(adRequest);
        img1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();

                Intent intcharee = new Intent(MainActivity.this, CharacterActivity.class);
                startActivity(intcharee);

            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intsec= new Intent(MainActivity.this, SentanceActivity.class);
                startActivity(intsec);
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intwrd = new Intent(MainActivity.this, WordActivity.class);
                startActivity(intwrd);
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intnum = new Intent(MainActivity.this, NumberActivity.class);
                startActivity(intnum);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intfh = new Intent(MainActivity.this, FreeHand.class);
                startActivity(intfh);

            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent intcharee = new Intent(MainActivity.this, CharacterActivity.class);
                startActivity(intcharee);

            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddsClass addsClass = new AddsClass(MainActivity.this);
                addsClass.showInterstitial();
                Intent inthindi = new Intent(MainActivity.this, HindiActivity.class);
                startActivity(inthindi);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.share) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Typing Testing");
                String shareMessage = "\nHi, Download this cool & fast performance Typing Testing App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {

            }
        }else if (id == R.id.rate) {

            try{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName())));
            }
            catch (ActivityNotFoundException e){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
            }
        }else if(id==R.id.more){
            // https://play.google.com/store/apps/developer?id=Appz+machine
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
        }

    }

