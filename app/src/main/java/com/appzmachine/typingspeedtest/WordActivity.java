package com.appzmachine.typingspeedtest;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class WordActivity extends AppCompatActivity {
    private TextView correct,wrong,accurancy,speed,wrod_textview;
    private EditText wrdeditview;
    private int correctCount = 0, wrongCount = 0;
    private  int Percent;
    private  int totalNoEntered =0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word);
        correct = findViewById(R.id.wrdcorrect);
        wrong = findViewById(R.id.wrdwrong);
        accurancy = findViewById(R.id.wrdaccurancy);
        speed = findViewById(R.id.wrdspeed);
        wrod_textview = findViewById(R.id.wrdtext);
        wrdeditview = findViewById(R.id.wrdtypehere);
        wrod_textview.setText(RandomString.getAlphaNumericString(5));
        wrdeditview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String s1 =  wrod_textview.getText().toString().trim();
                if (!s.toString().trim().equalsIgnoreCase("")) {

                    if (s.toString().length() > 4)

                        if (s1.equals(s.toString().trim())) {
                            totalNoEntered++;
                            correctCount++;

                        } else
                            {
                                totalNoEntered++;
                            wrongCount++;

                        }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().equalsIgnoreCase(""))
                    if (s.toString().length()>4)
                        new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            wrod_textview.setText(WordActivity.RandomString.getAlphaNumericString(5));
                            wrdeditview.setText("");
                            updateData();
                            calculate();
                        }
                    }, 1000);

            }
        });

    }

    private  void calculate(){
        Percent = (correctCount * 100) / totalNoEntered;
        accurancy.setText(String.valueOf(Percent + "%"));

    }
    private void updateData() {

//        boolean focussed =  wrdeditview.hasFocus();
//        if (focussed) {
//            wrdeditview.clearFocus();
//        }

        correct.setText(correctCount + "");
        wrong.setText(wrongCount + "");
       // accurancy.setText("data");
        speed.setText(correctCount+ " WPS");

    }



//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return super.onCreateOptionsMenu(item);
//    }
//
//
//
//    @Override
//    public boolean onOptionsItemSelected( MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.rate) {
//
//            Toast.makeText(this, "You click Rate", Toast.LENGTH_SHORT).show();
//        } else if (id == R.id.share) {
//            Toast.makeText(getApplicationContext(), "You click Share", Toast.LENGTH_SHORT).show();
//        }else if (id == R.id.more) {
//            Toast.makeText(getApplicationContext(), "You click  More", Toast.LENGTH_SHORT).show();
//        }
//        return super.onOptionsItemSelected(item);
//    }
public static class RandomString {

    // function to generate a random string of length n
    static  String getAlphaNumericString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "GPFAMCVDGPFAMCVDGPFAMCVDGPFAMCVDGPFAMCVDGPFAMCVDGPFAMCVD";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {


            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        // Get the size n
        int n = 20;

        // Get and display the alphanumeric string
        System.out.println(CharacterActivity.RandomString
                .getAlphaNumericString(n));
    }
}



}
