package com.appzmachine.typingspeedtest;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class FreeHand extends AppCompatActivity {
    private TextView wpm;
    private EditText editText;
    private int correctCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freehand);
//        Toolbar toolbar = findViewById(R.id.toolbar1);
//        setSupportActionBar(toolbar);
        wpm = findViewById(R.id.fntext1);
        editText = findViewById(R.id.fhedtext);
     //  String mess = editText.getText().toString();
       editText.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               final String s1 = editText.getText().toString().trim();
               if (!s.toString().trim().equalsIgnoreCase("")) {

                   if (s1.equals(s.toString().trim())) {

                       correctCount++;


                   } else {
//                       correctCount--;

                   }

               }
           }

           @Override
           public void afterTextChanged(Editable s) {
               if (!s.toString().trim().equalsIgnoreCase(""))

                       new Handler().postDelayed(new Runnable() {
                           @Override
                           public void run() {

                               editText.setText("");
                               updateData();

                           }
                       }, 1000);

           }
       });


        updateData();


    }
    private void updateData() {

//        boolean focussed = editText .hasFocus();
//        if (focussed) {
//            editText .clearFocus();
//        }

        wpm.setText(correctCount + "CPS");
       // wpm.setText(correctCount--+"cccc");
//        textView2.setText(wrongCount + "");
        // textView3.setText(Integer.toString(Pre));

        // textView3.setText(Integer.getInteger((String) Precent));

//        textView4.setText(correctCount+"CPM");

    }
}
