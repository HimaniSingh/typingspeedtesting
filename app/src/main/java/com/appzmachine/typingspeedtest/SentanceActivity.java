package com.appzmachine.typingspeedtest;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class SentanceActivity extends AppCompatActivity {
    private TextView correct, wrong, ac, speed, textView_main;
    private EditText editText_typehere;
    private int correctCount = 0, wrongCount = 0, arraySize;
    private int wordPosition = 0;
    private int Percent;
    private String str;
    String splitStr[];
    private boolean isTrueorFalse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentence);
//        Toolbar toolbar = findViewById(R.id.toolbar1);
//        setSupportActionBar(toolbar);
        correct = findViewById(R.id.sec_correct);
        wrong = findViewById(R.id.sec_wrong);
        ac = findViewById(R.id.sec_accurancy);
        speed = findViewById(R.id.sec_speed);
        textView_main = findViewById(R.id.sen_text);
        editText_typehere = findViewById(R.id.sen_edittext);
        str = textView_main.getText().toString();
        splitStr = str.split("\\s+");
        arraySize = splitStr.length;

        editText_typehere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String s1 = textView_main.getText().toString().trim();
                if (!s.toString().trim().equalsIgnoreCase("")) {
                    if (s.toString().length() == splitStr[wordPosition].length()) {

                        if (s.toString().equalsIgnoreCase(splitStr[wordPosition])) {
                            correctCount++;
                        } else {
                            wrongCount++;
                        }
                        wordPosition++;
                        isTrueorFalse = true;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().equalsIgnoreCase("")) {
                    if (isTrueorFalse) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isTrueorFalse = false;
//                                editText_typehere.setText("");
                                updateData();
                                calculate();

                            }
                        }, 300);
                    }
                }
            }


        });


    }

    private void calculate() {
        Percent = (correctCount * 100) / wordPosition;
        ac.setText(String.valueOf(Percent + "%"));

    }

    private void updateData() {

        correct.setText(correctCount + "");
        wrong.setText(wrongCount + "");
        speed.setText(correctCount + "CPS");

    }
}
