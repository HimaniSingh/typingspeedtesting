package com.appzmachine.typingspeedtest;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class HindiActivity extends AppCompatActivity {
    private TextView correct,wrong,accurancy,speed,hinditext;
    private EditText editText;
    private  int correctCount=0, wrongCount=0;
    private int Percent;
    private int totalNoEntered=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hindi);
//        Toolbar toolbar = findViewById(R.id.toolbar1);
//        setSupportActionBar(toolbar);
        correct = findViewById(R.id.hindi_correct);
        wrong = findViewById(R.id.hindi_wrong);
        accurancy = findViewById(R.id.hindi_accurancy);
        speed = findViewById(R.id.hindi_speed);
        hinditext = findViewById(R.id.hindi_text);
        editText = findViewById(R.id.hindi_edittext);
        hinditext.setText(RandomString.getAlphaNumericString(1));
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String s1 = hinditext.getText().toString().trim();
                if (!s.toString().trim().equalsIgnoreCase("")) {
                    totalNoEntered++;
                    if (s1.equals(s.toString().trim())) {
                        correctCount++;

                    } else {
                        wrongCount++;

                    }

                }
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().equalsIgnoreCase(""))
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hinditext.setText(HindiActivity.RandomString.getAlphaNumericString(1));
                            editText.setText("");
                            updateData();
                            calculate();
                        }
                    }, 500);

            }
        });



    }

    private void calculate(){
        Percent = (correctCount * 100) / totalNoEntered;
        accurancy.setText(String.valueOf(Percent + "%"));

    }
    private void updateData() {

//        boolean focussed = editText.hasFocus();
//        if (focussed) {
//            editText.clearFocus();
//        }

        correct.setText(correctCount + "");
        wrong.setText(wrongCount + "");

        speed.setText(correctCount+" CPS");

    }
    public static class RandomString {

        // function to generate a random string of length n
        static String getAlphaNumericString(int n) {

            // chose a Character random from this String
            String AlphaNumericString = "लखयपचब";

            // create StringBuffer size of AlphaNumericString
            StringBuilder sb = new StringBuilder(n);

            for (int i = 0; i < n; i++) {


                int index
                        = (int) (AlphaNumericString.length()
                        * Math.random());

                // add Character one by one in end of sb
                sb.append(AlphaNumericString
                        .charAt(index));
            }

            return sb.toString();
        }

        public static void main(String[] args) {

            // Get the size n
            int n = 20;

            // Get and display the alphanumeric string
            System.out.println(CharacterActivity.RandomString
                    .getAlphaNumericString(n));
        }
    }


}
