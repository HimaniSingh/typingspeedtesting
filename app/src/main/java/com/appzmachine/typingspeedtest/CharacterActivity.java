package com.appzmachine.typingspeedtest;


import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;



public class CharacterActivity extends AppCompatActivity {
    private TextView textView, textView1, textView2, textView3, textView4;
    EditText typehere;
    private  int correctCount = 0, wrongCount = 0;
    private int Precent;
    private int toatalNoEntered = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
//        Toolbar toolbar2 = findViewById(R.id.toolbarchar);
//        setSupportActionBar(toolbar2);
        textView = findViewById(R.id.chrtext);
        typehere = findViewById(R.id.typehere);
        textView1 = findViewById(R.id.char_correct);
        textView2 = findViewById(R.id.char_wrong);
        textView3 = findViewById(R.id.char_accurancy);
        textView4 = findViewById(R.id.char_speed);
        textView.setText(RandomString.getAlphaNumericString(1));

        typehere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String s1 = textView.getText().toString().trim();
                if (!s.toString().trim().equalsIgnoreCase("")) {
                     toatalNoEntered++;
                    if (s1.equals(s.toString().trim())) {
                        correctCount++;
                    } else {

                        wrongCount++;
                    }
                }
            }


            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().equalsIgnoreCase(""))

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(RandomString.getAlphaNumericString(1));
                            typehere.setText("");
                            changeValue();
                            updateData();
                        }
                    }, 500);

            }
        });





    }


    private void changeValue() {
         Precent = (correctCount * 100) / toatalNoEntered;
         textView3.setText(String.valueOf(Precent + "%"));
    }


    private void updateData() {

       textView1.setText(correctCount + "");
        textView2.setText(wrongCount + "");
        textView4.setText(correctCount + "CPS");

    }


    public static class RandomString {

        // function to generate a random string of length n
        static String getAlphaNumericString(int n) {

            // chose a Character random from this String
            String AlphaNumericString = "GPFAMCVD";

            // create StringBuffer size of AlphaNumericString
            StringBuilder sb = new StringBuilder(n);

            for (int i = 0; i < n; i++) {


                int index
                        = (int) (AlphaNumericString.length()
                        * Math.random());

                // add Character one by one in end of sb
                sb.append(AlphaNumericString
                        .charAt(index));
            }

            return sb.toString();
        }

        public static void main(String[] args) {

            // Get the size n
            int n = 20;

            // Get and display the alphanumeric string
            System.out.println(RandomString
                    .getAlphaNumericString(n));
        }
    }


}
