package com.appzmachine.typingspeedtest;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class NumberActivity extends AppCompatActivity {
    private TextView correct,wrong,speed,accurancy,numtext;
    private EditText edt_typehere;
    private int correctCount = 0, wrongCount = 0;
    private int Precent;
    private  int toatalNoEntered=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);
//        Toolbar toolbar = findViewById(R.id.toolbarnum);
//        setSupportActionBar(toolbar);
        correct = findViewById(R.id.num_correct);
        wrong = findViewById(R.id.num_wrong);
        accurancy = findViewById(R.id.num_accurancy);
        speed = findViewById(R.id.num_speed);
        numtext = findViewById(R.id.num_text);
        edt_typehere = findViewById(R.id.numedit);
        numtext.setText(RandomString.getAlphaNumericString(1));
        edt_typehere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String s1 = numtext.getText().toString().trim();
                if (!s.toString().trim().equalsIgnoreCase("")) {
                    toatalNoEntered++;
                    if (s1.equals(s.toString().trim())) {
                        correctCount++;

                    } else {
                        wrongCount++;

                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().equalsIgnoreCase(""))

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            numtext.setText(NumberActivity.RandomString.getAlphaNumericString(1));
                            edt_typehere.setText("");
                            updateData();
                            calculation();
                        }
                    }, 500);

            }
        });




    }

    private  void calculation(){
        Precent = (correctCount * 100) / toatalNoEntered;
        accurancy.setText(String.valueOf(Precent + "%"));


    }
    private void updateData() {

//        boolean focussed = edt_typehere.hasFocus();
//        if (focussed) {
//            edt_typehere.clearFocus();
//        }

        correct.setText(correctCount + "");
        wrong.setText(wrongCount + "");
      ///  accurancy.setText("data");
        speed.setText(correctCount+" NPS");

    }


//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return true;
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected( MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.rate) {
//
//            Toast.makeText(this, "You click Rate", Toast.LENGTH_SHORT).show();
//        } else if (id == R.id.share) {
//            Toast.makeText(getApplicationContext(), "You click Share", Toast.LENGTH_SHORT).show();
//        }else if (id == R.id.more) {
//            Toast.makeText(getApplicationContext(), "You click  More", Toast.LENGTH_SHORT).show();
//        }
//        return super.onOptionsItemSelected(item);
//    }
public static class RandomString {

    // function to generate a random string of length n
    static String getAlphaNumericString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "0123456789";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {


            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        // Get the size n
        int n = 20;

        // Get and display the alphanumeric string
        System.out.println(CharacterActivity.RandomString
                .getAlphaNumericString(n));
    }
}


}
